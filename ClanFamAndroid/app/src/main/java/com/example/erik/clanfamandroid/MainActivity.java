package com.example.erik.clanfamandroid;


import android.content.Context;
import android.content.Intent;
import android.icu.text.DateFormat;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;


import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;


public class MainActivity extends AppCompatActivity {

    Button videoButton;
    Button sendButton;
    EditText editText;
    private FirebaseListAdapter<ChatMessage> adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        videoButton = (Button) findViewById(R.id.videoButtonID);
        sendButton = (Button)  findViewById(R.id.sendButtonID);
        editText = (EditText) findViewById(R.id.editTextID);

        videoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent videoIntent = new Intent(MainActivity.this, LiveVideoActivity.class);
                videoIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(videoIntent);
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseDatabase.getInstance().getReference().push().setValue(new ChatMessage(editText.getText().toString(),
                        FirebaseAuth.getInstance().getCurrentUser().getEmail()));
                editText.setText("");
                editText.requestFocus();
                displayChatMessage();
            }
        });


    }

    private void displayChatMessage() {

        ListView listOfMessage = (ListView)findViewById(R.id.listID);
        adapter = new FirebaseListAdapter<ChatMessage>(this,ChatMessage.class,R.layout.list_item,FirebaseDatabase.getInstance().getReference())
        {
            @Override
            protected void populateView(View v, ChatMessage model, int position) {

                //Get references to the views of list_item.xml
                TextView messageText, messageUser, messageTime;
                messageText = (TextView) v.findViewById(R.id.message_text);
                messageUser = (TextView) v.findViewById(R.id.message_user);
                messageTime = (TextView) v.findViewById(R.id.message_time);

                messageText.setText(model.getMessageText());
                messageUser.setText(model.getMessageUser());


            }
        };
        listOfMessage.setAdapter(adapter);
    }


}
