package com.example.erik.clanfamandroid;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LoginFragment extends Fragment {

    private EditText emailText;
    private EditText userNameText;
    private EditText passwordText;


    private FirebaseAuth mAuth;
    private ProgressDialog mProgress;

    private DatabaseReference mDatabaseUsers;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mAuth = FirebaseAuth.getInstance();
        mDatabaseUsers = FirebaseDatabase.getInstance().getReference().child("users");


        mProgress = new ProgressDialog(getContext());
        View rootview = inflater.inflate(R.layout.loginfragment_layout, container, false);
        emailText = (EditText) rootview.findViewById(R.id.email_id);
        userNameText = (EditText) rootview.findViewById(R.id.username_id);
        passwordText = (EditText) rootview.findViewById(R.id.password_id);
        Button signupButton = (Button) rootview.findViewById(R.id.signup_id);


        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignUp();
            }
        });


        return rootview;
    }

    private void SignUp() {
        final String userName = userNameText.getText().toString();
        final String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
            mProgress.setMessage("Signing Up....");
            mProgress.show();

            mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {


                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {

                        @SuppressWarnings("ConstantConditions") String user_id = mAuth.getCurrentUser().getUid();

                        DatabaseReference current_user_db = mDatabaseUsers.child(user_id);

                        current_user_db.child("username").setValue(userName);
                        current_user_db.child("email").setValue(email);
                        current_user_db.child("uid").setValue(mAuth.getCurrentUser().getUid());

                        mProgress.dismiss();

                        Intent mainIntent = new Intent(getContext(), MainActivity.class);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        getActivity().finish();
                        startActivity(mainIntent);


                    }
                }
            });

        }
    }




}

